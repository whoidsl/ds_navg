/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ivaughn on 5/16/19.
//

#include "InsStatus.h"
#include "ui_InsStatus.h"
#include <QHBoxLayout>
#include <QPushButton>
#include <QSettings>
#include <ds_sensor_msgs/PhinsConfig.h>
#include <dslmap/BeaconLayer.h>
#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>
#include <dslmap/MarkerSymbol.h>
#include <dslmap/Proj.h>
#include <ros/node_handle.h>
#include <ros/ros.h>

namespace navg {
namespace plugins {
namespace ros_insstatus {

Q_LOGGING_CATEGORY(plugin_ros_insstatus, "navg.plugins.ros.ins_status")

InsStatus::InsStatus(QWidget *parent)
    : QWidget(parent), m_seconds_since(-1),
      ui(std::make_unique<Ui::InsStatus>()) {
  ui->setupUi(this);
  sub_manager = nullptr;
}

InsStatus::~InsStatus() {
  if (sub_manager) {
    delete sub_manager;
  }
}

void InsStatus::setupPlugin() {

  widget2_ = new QWidget(); // why is this even used? want to remove -tj 2022

  // tab_scroll_phins = new QScrollArea(widget_);

  // phinsins_controller_ = new ds_rqt_widgets::PhinsInsController("PHINS",
  // "/alvin/sensors/phinsins",//"/alvin/sensors/phinsins",
  //                                                 ds_rqt_widgets::PhinsIns::COMPACT_C7,
  //                                                 ds_rqt_widgets::PhinsIns::GPS1
  //                                                 |
  //                                                 ds_rqt_widgets::PhinsIns::DVL1
  //                                                 |
  //                                                 ds_rqt_widgets::PhinsIns::DVL2
  //                                                 |
  //                                                 ds_rqt_widgets::PhinsIns::DVLWT2
  //                                                 |
  //                                                 ds_rqt_widgets::PhinsIns::DEPTH
  //                                                 |
  //                                                 ds_rqt_widgets::PhinsIns::USBL,
  //                                                 tab_scroll_phins);

  phinsins_controller_compact_ = new ds_rqt_widgets::PhinsInsControllerCompact(
      "PHINS",
      phinsins_node_name_
          .toStdString(),
      sensor_modes_enabled_,
      zupt_modes_enabled_,
      altitude_modes_enabled_,
      dive_modes_enabled_,
      startup_modes_enabled_,
      usbl_modes_enabled_,
      status_msgs_enabled_,
      ds_rqt_widgets::PhinsIns::PHINS3,
      ds_rqt_widgets::PhinsIns::GPS1 | ds_rqt_widgets::PhinsIns::GPS2 |
          ds_rqt_widgets::PhinsIns::DVL1 | ds_rqt_widgets::PhinsIns::DVL2 |
          ds_rqt_widgets::PhinsIns::DVLWT2 | ds_rqt_widgets::PhinsIns::DEPTH |
          ds_rqt_widgets::PhinsIns::USBL); // tab_scroll_phins);

  // nh = *node_handle;
  ros::NodeHandle nh;
  sub_manager = new ds_rqt_widgets::QtRosSubscriptionManager(nh);
  sub_phinsbin = ds_rqt_widgets::SubscribeToTopic<ds_sensor_msgs::PhinsStdbin3>(
      nh, "PhinsBin", phinsins_node_name_.toStdString() + "/phinsbin", 10);
  sub_phinsfogest =
      ds_rqt_widgets::SubscribeToTopic<geometry_msgs::Vector3Stamped>(
          nh, "FogBias", phinsins_node_name_.toStdString() + "/fogest", 10);
  sub_phinsaccest =
      ds_rqt_widgets::SubscribeToTopic<geometry_msgs::Vector3Stamped>(
          nh, "AccBias", phinsins_node_name_.toStdString() + "/accest", 10);
  sub_phinsparams =
      ds_rqt_widgets::SubscribeToTopic<dynamic_reconfigure::Config>(
          nh, "PhinsConfig",
          phinsins_node_name_.toStdString() + "/parameter_updates", 10);
  sub_manager->addSubscription(sub_phinsbin);
  sub_manager->addSubscription(sub_phinsfogest);
  sub_manager->addSubscription(sub_phinsaccest);
  sub_manager->addSubscription(sub_phinsparams);

  sub_widget =
      new ds_rqt_widgets::QtRosSubscriptionWidget(widget2_, nh, sub_manager);

  // Hacking in a service client because ds_rqt_widgets dynamicreconfigure
  // client just crashes everything. cool.
  phins_dr_srv = nh.serviceClient<dynamic_reconfigure::Reconfigure>(
      phinsins_node_name_.toStdString() + "/set_parameters");
  qDebug(plugin_ros_insstatus) << "Dynamic reconfigure client created";

  reset_phins_srv = nh.serviceClient<ds_core_msgs::VoidCmd>(
      phinsins_node_name_.toStdString() + "/phins_restart");
  reinit_phins_srv = nh.serviceClient<ds_core_msgs::StringCmd>(
      phinsins_node_name_.toStdString() + "/phins_reinit");
  phins_usbl_mode_srv = nh.serviceClient<ds_core_msgs::StringCmd>(
      phinsins_node_name_.toStdString() + "/phins_usbl_mode");


  connect(sub_phinsbin, SIGNAL(received(AnyMessage)),
          phinsins_controller_compact_, SLOT(updatePhinsbin(AnyMessage)),
          Qt::QueuedConnection);
  connect(sub_phinsfogest, SIGNAL(received(AnyMessage)),
          phinsins_controller_compact_, SLOT(updateGyroBias(AnyMessage)),
          Qt::QueuedConnection);
  connect(sub_phinsaccest, SIGNAL(received(AnyMessage)),
          phinsins_controller_compact_, SLOT(updateAccelBias(AnyMessage)),
          Qt::QueuedConnection);
  connect(sub_phinsparams, SIGNAL(received(AnyMessage)), this,
          SLOT(updateConfig(AnyMessage)), Qt::QueuedConnection);
  connect(this, SIGNAL(updatedPhinsConfig(AnyMessage)),
          phinsins_controller_compact_, SLOT(updateConfig(AnyMessage)),
          Qt::QueuedConnection);
  connect(phinsins_controller_compact_, SIGNAL(newPhinsConfig(AnyMessage)),
          this, SLOT(setPhinsConfiguration(AnyMessage)), Qt::QueuedConnection);


  ui->plugin_layout->addWidget(phinsins_controller_compact_);
  // main_layout->addWidget(widget2_); // This is for the topics but we should
  // not have this in ops

  // Connect reset and reinit slots
  connect(phinsins_controller_compact_,
          &ds_rqt_widgets::PhinsInsControllerCompact::resetPhins, this,
          &InsStatus::resetPhins);

  connect(phinsins_controller_compact_,
          &ds_rqt_widgets::PhinsInsControllerCompact::reinitPhins, this,
          &InsStatus::reinitPhins);

  connect(phinsins_controller_compact_,
          &ds_rqt_widgets::PhinsInsControllerCompact::updateUsblMode, this,
          &InsStatus::updateUsblMode);
}

// Get the latest dynamic_reconfigure::Config msg and convert to
// ds_sensor_msgs::PhinsConfig, then emit updated msg for phinsins_controller_
void InsStatus::updateConfig(AnyMessage phinscfg) {
  // qDebug(plugin_ros_insstatus)<<"received phins config msg";
  dynamic_reconfigure::Config dynconf_update =
      *(phinscfg.as<dynamic_reconfigure::Config>());
  ds_sensor_msgs::PhinsConfig config_msg;
  for (auto &param : dynconf_update.ints) {
    // std::cout<<"Param is "<< param<<' '<<std::endl;
    std::string cur_name = param.name;
    int cur_val = param.value;
    if (cur_name == "zuptMode") {
      config_msg.zuptMode = cur_val;
      continue;
    } else if (cur_name == "altMode") {
      config_msg.altMode = cur_val;
      continue;
    } else if (cur_name == "gps1Rejection") {
      config_msg.gps1Rejection = cur_val;
      continue;
    } else if (cur_name == "gps2Rejection") {
      config_msg.gps2Rejection = cur_val;
      continue;
    } else if (cur_name == "dvl1Rejection") {
      config_msg.dvl1Rejection = cur_val;
      continue;
    } else if (cur_name == "dvl2Rejection") {
      config_msg.dvl2Rejection = cur_val;
      continue;
    } else if (cur_name == "dvlWt1Rejection") {
      config_msg.dvlWt1Rejection = cur_val;
      continue;
    } else if (cur_name == "dvlWt2Rejection") {
      config_msg.dvlWt2Rejection = cur_val;
      continue;
    } else if (cur_name == "depthRejection") {
      config_msg.depthRejection = cur_val;
      continue;
    } else if (cur_name == "usblRejection") {
      config_msg.usblRejection = cur_val;
      continue;
    } else if (cur_name == "diveMode") {
      config_msg.diveMode = cur_val;
      continue;
    } else {
      qDebug(plugin_ros_insstatus) << "INVALID PHINS CONFIG PARAM RECEIVED";
      std::cout << "INVALID PARAM IS " << param << ": " << cur_val << std::endl;
    }
  }

  boost::shared_ptr<const ds_sensor_msgs::PhinsConfig> config_to_update(
      new const ds_sensor_msgs::PhinsConfig(config_msg));
  emit updatedPhinsConfig(AnyMessage(config_to_update));
}

// Get updated phins config, convert to dynamic_reconfigure request and call
// service
bool InsStatus::setPhinsConfiguration(AnyMessage phinscfg) {
  qDebug(plugin_ros_insstatus) << "Phins config change requested";
  dynamic_reconfigure::ReconfigureRequest srv_req;
  dynamic_reconfigure::ReconfigureResponse srv_resp;
  dynamic_reconfigure::IntParameter phins_param;
  dynamic_reconfigure::Config conf;
  ds_sensor_msgs::PhinsConfig dynconf_phins;
  dynconf_phins = *phinscfg.as<ds_sensor_msgs::PhinsConfig>();

  phins_param.name = "diveMode";
  phins_param.value = dynconf_phins.diveMode;

  if (phins_param.name == "diveMode" && phins_param.value == 1) {
    // in water mode
    phins_param.name = "diveMode";
    phins_param.value = 1;
    conf.ints.push_back(phins_param);

    phins_param.name = "zuptMode";
    phins_param.value = 0;
    conf.ints.push_back(phins_param);

    phins_param.name = "altMode";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "gps1Rejection";
    phins_param.value = 1;
    conf.ints.push_back(phins_param);

    phins_param.name = "gps2Rejection";
    phins_param.value = 1;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvl1Rejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvl2Rejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvlWt2Rejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "depthRejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "usblRejection";
    phins_param.value = 0;
    conf.ints.push_back(phins_param);
  } else if (phins_param.name == "diveMode" && phins_param.value == 0) {
    // on deck mode
    phins_param.name = "diveMode";
    phins_param.value = 0;
    conf.ints.push_back(phins_param);

    phins_param.name = "zuptMode";
    phins_param.value = 0;
    conf.ints.push_back(phins_param);

    phins_param.name = "altMode";
    phins_param.value = 1;
    conf.ints.push_back(phins_param);

    phins_param.name = "gps1Rejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "gps2Rejection";
    phins_param.value = 1;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvl1Rejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvl2Rejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvlWt2Rejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "depthRejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "usblRejection";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);
  } else {
    phins_param.name = "diveMode";
    phins_param.value = 2;
    conf.ints.push_back(phins_param);

    phins_param.name = "zuptMode";
    phins_param.value = dynconf_phins.zuptMode;
    conf.ints.push_back(phins_param);

    phins_param.name = "altMode";
    phins_param.value = dynconf_phins.altMode;
    conf.ints.push_back(phins_param);

    phins_param.name = "gps1Rejection";
    phins_param.value = dynconf_phins.gps1Rejection;
    conf.ints.push_back(phins_param);

    phins_param.name = "gps2Rejection";
    phins_param.value = dynconf_phins.gps2Rejection;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvl1Rejection";
    phins_param.value = dynconf_phins.dvl1Rejection;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvl2Rejection";
    phins_param.value = dynconf_phins.dvl2Rejection;
    conf.ints.push_back(phins_param);

    phins_param.name = "dvlWt2Rejection";
    phins_param.value = dynconf_phins.dvlWt2Rejection;
    conf.ints.push_back(phins_param);

    phins_param.name = "depthRejection";
    phins_param.value = dynconf_phins.depthRejection;
    conf.ints.push_back(phins_param);

    phins_param.name = "usblRejection";
    phins_param.value = dynconf_phins.usblRejection;
    conf.ints.push_back(phins_param);
  }

  srv_req.config = conf;
  if (!phins_dr_srv.call(srv_req, srv_resp)) {
    qDebug(plugin_ros_insstatus) << "Unable to call phins parameter service";
    return false;
  }
  conf.ints.clear();
  return true;
}

void InsStatus::connectPlugin(const QObject *plugin,
                              const QJsonObject &metadata) {
  if (!plugin) {
    return;
  }

  const auto name =
      metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  // connect to ROS backend plugin
  if (name == "dsros_Backend") {
    qDebug(plugin_ros_insstatus)
        << "Connecting INS STATUS plugin to ROS backend";
    node_handle.reset(new ros::NodeHandle());
  }
}

void InsStatus::setMapView(QPointer<dslmap::MapView> view) {
  m_view = view;
}

void InsStatus::loadSettings(QSettings *settings) {

  auto value = settings->value("phinsins_node_name");
  auto ok = true;
  if (value.isValid()) {
    const auto valid_val = value.toString();
    if (ok) {
      qDebug() << valid_val;
      phinsins_node_name_ = valid_val;
    }
  } else {
    phinsins_node_name_ = "/jason/sensors/phinsins";
  }
  sensor_modes_enabled_ = false;
  zupt_modes_enabled_ = false;
  altitude_modes_enabled_ = false;
  dive_modes_enabled_ = true;
  startup_modes_enabled_ = true;
  usbl_modes_enabled_ = false;
  status_msgs_enabled_ = true;
  /*
  auto sensor_modes_val = settings->value("sensor_modes_enabled");
  auto sensor_modes_ok = true;
  if (sensor_modes_val.isValid()) {
    const auto valid_val = sensor_modes_val.toBool();
    if (sensor_modes_ok) {
      qDebug() << valid_val;
      sensor_modes_enabled_ = valid_val;
    }
  } else {
    qDebug() << "Setting INS sensor modes to false";
    sensor_modes_enabled_ = false;
  }

  auto zupt_modes_val = settings->value("zupt_modes_enabled");
  auto zupt_modes_ok = true;
  if (zupt_modes_val.isValid()) {
    const auto valid_val = zupt_modes_val.toBool();
    if (zupt_modes_ok) {
      qDebug() << valid_val;
      zupt_modes_enabled_ = valid_val;
    }
  } else {
    qDebug() << "Setting INS ZUPT modes to false";
    zupt_modes_enabled_ = false;
  }

  auto altitude_modes_val = settings->value("altitude_modes_enabled");
  auto altitude_modes_ok = true;
  if (altitude_modes_val.isValid()) {
    const auto valid_val = altitude_modes_val.toBool();
    if (altitude_modes_ok) {
      qDebug() << valid_val;
      altitude_modes_enabled_ = valid_val;
    }
  } else {
    qDebug() << "Setting INS altitude modes to false";
    altitude_modes_enabled_ = false;
  }

  auto dive_modes_val = settings->value("dive_modes_enabled");
  auto dive_modes_ok = true;
  if (dive_modes_val.isValid()) {
    const auto valid_val = dive_modes_val.toBool();
    if (dive_modes_ok) {
      qDebug() << valid_val;
      dive_modes_enabled_ = valid_val;
    }
  } else {
    qDebug() << "Setting INS dive modes to false";
    dive_modes_enabled_ = true;
  }

  auto startup_modes_val = settings->value("startup_modes_enabled");
  auto startup_modes_ok = true;
  if (startup_modes_val.isValid()) {
    const auto valid_val = startup_modes_val.toBool();
    if (startup_modes_ok) {
      qDebug() << valid_val;
      startup_modes_enabled_ = valid_val;
    }
  } else {
    qDebug() << "Setting INS startup modes to false";
    startup_modes_enabled_ = true;
  }

  auto usbl_modes_val = settings->value("usbl_modes_enabled");
  auto usbl_modes_ok = true;
  if (usbl_modes_val.isValid()) {
    const auto valid_val = usbl_modes_val.toBool();
    if (usbl_modes_ok) {
      qDebug() << valid_val;
      usbl_modes_enabled_ = valid_val;
    }
  } else {
    qDebug() << "Setting INS usbl modes to false";
    usbl_modes_enabled_ = false;
  }

  auto status_msgs_val = settings->value("status_msgs_enabled");
  auto status_msgs_ok = true;
  if (status_msgs_val.isValid()) {
    const auto valid_val = status_msgs_val.toBool();
    if (status_msgs_ok) {
      qDebug() << valid_val;
      status_msgs_enabled_ = valid_val;
    }
  } else {
    qDebug() << "Setting INS status msgs modes to false";
    status_msgs_enabled_ = true;
  }
  */
  setupPlugin();
}

void InsStatus::saveSettings(QSettings &settings) {
  settings.setValue("persist", 1);
  settings.setValue("phinsins_node_name", phinsins_node_name_);
  settings.setValue("sensor_modes_enabled", sensor_modes_enabled_);
  settings.setValue("zupt_modes_enabled", zupt_modes_enabled_);
  settings.setValue("altitude_modes_enabled", altitude_modes_enabled_);
  settings.setValue("dive_modes_enabled", dive_modes_enabled_);
  settings.setValue("startup_modes_enabled", startup_modes_enabled_);
  settings.setValue("usbl_modes_enabled", usbl_modes_enabled_);
  settings.setValue("status_msgs_enabled", status_msgs_enabled_);
}

QList<QAction *> InsStatus::pluginMenuActions() { return {}; }

void InsStatus::resetPhins() {
  ROS_WARN_STREAM("Resetting PHINS at user request!");
  ds_core_msgs::VoidCmd srv;
  if (!reset_phins_srv.call(srv)) {
    ROS_ERROR_STREAM("ERROR: UNABLE TO RESET PHINS!");
  }
}

void InsStatus::reinitPhins(int startposval) {
  ROS_WARN_STREAM("Reinit PHINS to: " << startposval << " at user request!");
  ds_core_msgs::StringCmd srv;
  srv.request.cmd = std::to_string(startposval);
  if (!reinit_phins_srv.call(srv)) {
    ROS_ERROR_STREAM("ERROR: UNABLE TO REINIT PHINS!");
  }
}

void InsStatus::updateUsblMode(int covariance_scalar_val) {
  ROS_WARN_STREAM("Update PHINS USBL Covariance scalar to: "
                  << covariance_scalar_val << " at user request!");
  ds_core_msgs::StringCmd srv;
  srv.request.cmd = std::to_string(covariance_scalar_val);
  if (!phins_usbl_mode_srv.call(srv)) {
    ROS_ERROR_STREAM(
        "ERROR: UNABLE TO UPDATE PHINS USBL COVARIANCE SCALAR VAL!");
  }
}

} // namespace ros_insstatus
} // namespace plugins
} // namespace navg

// [dsros_InsStatus]
// phinsins_node_name = /alvin/sensors/phinsins
// persist = 1

// requires:
// #[dsros_Backend]
// #loadme = 1

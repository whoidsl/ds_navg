/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ivaughn on 5/16/19.
//

#ifndef DS_NAVG_ROS_INSSTATUS_H
#define DS_NAVG_ROS_INSSTATUS_H

#include <dslmap/BeaconLayer.h>
#include <dslmap/MapItem.h>
#include <dslmap/Proj.h>
#include <navg/CorePluginInterface.h>
#include <dslmap/MapLayer.h>
#include <ros/ros.h>
#include <ros/service_client.h>
#include <QAction>
#include <QHash>
#include <QJsonObject>
#include <QLabel>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QUuid>
#include <QWidget>
#include <QScrollArea>
#include <memory>

#include <dynamic_reconfigure/Config.h>
#include <dynamic_reconfigure/IntParameter.h>
#include <dynamic_reconfigure/client.h>

#include <ds_rqt_widgets/QtRosSubscriptionManager.h>
#include <ds_rqt_widgets/QtRosSubscriptionWidget.h>
#include <ds_rqt_widgets/QtRosSubscription.h>
#include <ds_rqt_widgets/QtRosDynamicReconfigureClient.h>
#include <ds_rqt_widgets/PhinsTable.h>
#include <ds_rqt_widgets/PhinsInsTab.h>
#include <ds_rqt_widgets/PhinsInsController.h>
#include <ds_rqt_widgets/PhinsInsControllerCompact.h>
#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <ds_sensor_msgs/PhinsConfig.h>
#include <ds_core_msgs/VoidCmd.h>
#include <ds_core_msgs/StringCmd.h>

namespace navg {
namespace plugins {
namespace ros_insstatus {

enum class ControlType
{
  OFF,
  AUTO,
  FORCED,
};

enum class InputType
{
  GPS,
  DVL1,
  DVL2,
  SVP,
  DEPTH,
  USBL,
};

namespace Ui {
class InsStatus;
}
using LayerHash = QHash<QUuid, dslmap::MapLayer *>;

class InsStatus : public QWidget, public navg::CorePluginInterface {
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "InsStatus.json")

public:
  explicit InsStatus(QWidget *parent = nullptr);
  ~InsStatus() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings *settings) override;
  void saveSettings(QSettings &settings) override;

  // This plugin connects to DS_ROS_BACKEND
  void connectPlugin(const QObject *plugin,
                     const QJsonObject &metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction *> pluginMenuActions() override;
  QList<QAction *> pluginToolbarActions() override { return {}; }

signals:
  void updatedPhinsConfig(AnyMessage config);

public slots:
  bool setPhinsConfiguration(AnyMessage msg);
  void updateConfig(AnyMessage config);
  void phinsConfigReceived(dynamic_reconfigure::Config configmsg);
  void resetPhins(void);
  void reinitPhins(int startposval);
  void updateUsblMode(int covariance_scalar_val);

protected:
  void setupPlugin();
  void setupWidget();
  void setupConnections();
  void shutdownConnections();

private:
  std::unique_ptr<Ui::InsStatus> ui;
  QPointer<dslmap::MapView> m_view;
  QString current_selected_topic_qstring;
  QString initial_topic;

  QString phinsins_node_name_;
  bool sensor_modes_enabled_;
  bool zupt_modes_enabled_;
  bool altitude_modes_enabled_;
  bool dive_modes_enabled_;
  bool startup_modes_enabled_;
  bool usbl_modes_enabled_;
  bool status_msgs_enabled_;
  
  ros::NodeHandlePtr node_handle;
  //ros::DynamicReconfigureClient phins_param_srv;
  //dynamic_reconfigure::Client phins_param_srv;
  std::string phins_param_topicname;
  int m_seconds_since;

  ///This exists to confirm if when a Beacon Layer is created, no projection 
  //was set in scene beforehand. 
  bool wasNoSceneProjection=false;

  void removeDuplicateTopics(std::vector<std::string> &topics);

  QVBoxLayout* main_layout;
  //QScrollArea *tab_scroll_phins;
  //QWidget *tab_scroll_phins;

  QString phinsbin_topic;
  QString phinsaccest_topic;
  QString phinsfogest_topic;
  QString phinsconfig_topic;
  QString reset_service;
  QString name_dynamicreconfigure;

  QWidget* widget_;
  QWidget* widget2_;

  //ros::NodeHandle node_handle;
  ros::NodeHandle nh;
  ros::ServiceClient phins_dr_srv;
  ros::ServiceClient reset_phins_srv;
  ros::ServiceClient reinit_phins_srv;
  ros::ServiceClient phins_usbl_mode_srv;

  ds_rqt_widgets::QtRosSubscriptionPopout *sub_popout;
  ds_rqt_widgets::QtRosSubscriptionManager *sub_manager;
  ds_rqt_widgets::QtRosSubscriptionWidget *sub_widget;

  ds_rqt_widgets::QtRosSubscription *sub_phinsbin;
  ds_rqt_widgets::QtRosSubscription *sub_phinsfogest;
  ds_rqt_widgets::QtRosSubscription *sub_phinsaccest;
  ds_rqt_widgets::QtRosSubscription *sub_phinsparams;
  ds_rqt_widgets::QtRosDynamicReconfigureClient *dyn_config;
  ds_rqt_widgets::PhinsTable* phins_tab_;
  ds_rqt_widgets::PhinsInsTab* phinsins_tab_;
  ds_rqt_widgets::PhinsInsController* phinsins_controller_;
  ds_rqt_widgets::PhinsInsControllerCompact* phinsins_controller_compact_;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_ros_insstatus)
} // namespace ros_insstatus
} // namespace plugins
} // namespace navg

#endif // DS_NAVG_ROS_INSSTATUS_H

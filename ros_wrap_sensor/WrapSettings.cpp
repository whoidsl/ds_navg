/**
 * Copyright 2022 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "WrapSettings.h"
#include "ui_WrapSettings.h"

namespace navg {
namespace plugins {
namespace ros_wrap {

WrapSettings::WrapSettings(QWidget *parent)
    : QDialog(parent), ui(std::make_unique<Ui::WrapSettings>()) {
  ui->setupUi(this);
}

WrapSettings::~WrapSettings() = default;

void WrapSettings::setport1(int port) { ui->port1->setValue(port); }
void WrapSettings::setport2(int port) { ui->port2->setValue(port); }
void WrapSettings::setport3(int port) { ui->port3->setValue(port); }

int WrapSettings::getPort1() { return ui->port1->value(); }

int WrapSettings::getPort2() { return ui->port2->value(); }

int WrapSettings::getPort3() { return ui->port3->value(); }

} // namespace ros_wrap
} // namespace plugins
} // namespace navg

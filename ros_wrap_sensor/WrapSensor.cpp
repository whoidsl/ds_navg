/**
 * Copyright 2022 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 09/07/22
//

#include "WrapSensor.h"
#include "WrapSettings.h"
#include "ui_WrapSensor.h"
#include <dslmap/Proj.h>
#include <ros/node_handle.h>
#include <ros/ros.h>

namespace navg {
namespace plugins {
namespace ros_wrap {

Q_LOGGING_CATEGORY(plugin_ros_wrap, "navg.plugins.ros.wrap")

WrapSensor::WrapSensor(QWidget *parent)
    : QWidget(parent), ui(std::make_unique<Ui::WrapSensor>()),
      m_action_show_settings(new QAction("&Settings", this))

{
  ui->setupUi(this);
  ui->status_box->setStyleSheet("QGroupBox { border:1px solid grey} ");
  sub_manager = nullptr;

  connect(m_action_show_settings, &QAction::triggered, this,
          &WrapSensor::showSettingsDialog);

  connect(this, &WrapSensor::settingsUpdated, this,
          &WrapSensor::populateSmsPortDropdown);

  connect(ui->request_button, &QPushButton::clicked, this,
          &WrapSensor::requestWraps);

  connect(ui->fake, &QPushButton::clicked, this, &WrapSensor::updateFakeCount);

  // debug button. used for only testing. Hide normally.
  ui->fake->hide();
}

WrapSensor::~WrapSensor() {}

void WrapSensor::connectPlugin(const QObject *plugin,
                               const QJsonObject &metadata) {
  if (!plugin) {
    return;
  }

  const auto name =
      metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  // connect to ROS backend plugin
  if (name == "dsros_Backend") {
    qDebug(plugin_ros_wrap) << "Connecting wrap sensor plugin to ROS backend";
    node_handle.reset(new ros::NodeHandle());
    setupConnections();
  }
}

void WrapSensor::setMapView(QPointer<dslmap::MapView> view) { m_view = view; }

void WrapSensor::loadSettings(QSettings *settings) {

  auto value = settings->value("port1");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port1 = valid_val;
    }
  }
  value = settings->value("port2");
  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port2 = valid_val;
    }
  }
  value = settings->value("port3");
  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port3 = valid_val;
    }
  }

  value = settings->value("wrap_sensor_node_name");
  ok = true;
  if (value.isValid()) {
    const auto valid_val = value.toString();
    if (ok) {
      wrap_sensor_node_name = valid_val;
    }
  }

  emit settingsUpdated();
}

void WrapSensor::saveSettings(QSettings &settings) {
  settings.setValue("persist", 1);
  settings.setValue("port1", port1);
  settings.setValue("port2", port2);
  settings.setValue("port3", port3);
  settings.setValue("wrap_sensor_node_name", wrap_sensor_node_name);
}

QList<QAction *> WrapSensor::pluginMenuActions() {
  return {m_action_show_settings};
}

void WrapSensor::showSettingsDialog() {

  QScopedPointer<WrapSettings> dialog(new WrapSettings);

  dialog->setport1(port1);
  dialog->setport2(port2);
  dialog->setport3(port3);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  port1 = dialog->getPort1();
  port2 = dialog->getPort2();
  port3 = dialog->getPort3();

  emit settingsUpdated();
}

void WrapSensor::setupConnections() {
  // debug--delete publisher stuff
  // chatter_pub = nh.advertise<std_msgs::String>("chatter", 1000);
  sub_manager = new ds_rqt_widgets::QtRosSubscriptionManager(nh);
  sub_wraps = ds_rqt_widgets::SubscribeToTopic<ds_sensor_msgs::WrapSensor>(
      nh, "WrapStatus", wrap_sensor_node_name.toStdString() + "/wrap_status",
      10);
  request_wraps_srv = nh.serviceClient<ds_core_msgs::StringCmd>(
      wrap_sensor_node_name.toStdString() + "/query_wraps");

  sub_manager->addSubscription(sub_wraps);

  connect(sub_wraps, SIGNAL(received(AnyMessage)), this,
          SLOT(updateWrapCount(AnyMessage)), Qt::QueuedConnection);
}

void WrapSensor::populateSmsPortDropdown() {
  ui->sms_dropdown->clear();

  if (port1 != 0) {
    ui->sms_dropdown->addItem(QString::number(port1));
  }
  if (port2 != 0) {
    ui->sms_dropdown->addItem(QString::number(port2));
  }
  if (port3 != 0) {
    ui->sms_dropdown->addItem(QString::number(port3));
  }
}

void WrapSensor::updateWrapCount(AnyMessage wraps) {
  auto msg = *(wraps.as<ds_sensor_msgs::WrapSensor>());
  double wrap_count = msg.wraps_counter;
  double wrap_heading = msg.wraps_heading;
  qDebug(plugin_ros_wrap) << "Updating wrapcount on topic "
                          << wrap_sensor_node_name << "/wrap_status"
                          << " with value " << wrap_count;
  qDebug(plugin_ros_wrap) << "Updating wrapheading on topic "
                          << wrap_sensor_node_name << "/wrap_status"
                          << " with value " << wrap_heading;
  ui->wrapcount_label->setText(QString::number(wrap_count));
  ui->wrapheading_label->setText(QString::number(wrap_heading));

  m_time_last_received = QDateTime::currentDateTime();

  if (!age_timer)
    age_timer.reset(new QTimer(this));
  connect(age_timer.data(), &QTimer::timeout, this, &WrapSensor::handleTimer);
  age_timer->start(1000);
}

void WrapSensor::updateFakeCount() {
  /*
  std_msgs::String msg;
  std::stringstream ss;
  ss << "hii";
  msg.data = ss.str();
  chatter_pub.publish(msg);
  */

  double wrap_count = 12;
  double wrap_heading = 75;

  qDebug(plugin_ros_wrap) << "Updating wrapcount on topic "
                          << wrap_sensor_node_name << "/wrap_status"
                          << " with value " << wrap_count;
  qDebug(plugin_ros_wrap) << "Updating wrapheading on topic "
                          << wrap_sensor_node_name << "/wrap_status"
                          << " with value " << wrap_heading;

  ui->wrapcount_label->setText(QString::number(wrap_count));
  ui->wrapheading_label->setText(QString::number(wrap_heading));

  m_time_last_received = QDateTime::currentDateTime();

  if (!age_timer)
    age_timer.reset(new QTimer(this));
  connect(age_timer.data(), &QTimer::timeout, this, &WrapSensor::handleTimer);
  age_timer->start(1000);
}

void WrapSensor::requestWraps() {
  auto current_port = ui->sms_dropdown->currentText();
  if (current_port.isEmpty())
    return;

  ROS_WARN_STREAM("Sending service call to query_wraps topic "
                  << wrap_sensor_node_name.toStdString() << " with address "
                  << current_port.toStdString());
  ROS_WARN_STREAM(wrap_sensor_node_name.toStdString() << "/query_wraps");
  ds_core_msgs::StringCmd srv;
  srv.request.cmd = current_port.toStdString();
  if (!request_wraps_srv.call(srv)) {
    ROS_ERROR_STREAM("ERROR: UNABLE TO REQUEST WRAP SENSOR STATUS");
    return;
  }

  // reset all previous wrap stuff. could be from different avtrak address
  // not going to track all the potential edge case differences

  if (current_port != last_requested_port) {
    ui->status_box->setTitle("Wrap Status(" + current_port + ")");
    age_timer.reset(new QTimer(this));
    ui->ss_update_label->setText("Unknown");
    ui->wrapcount_label->setText("Unknown");
    ui->wrapheading_label->setText("Unknown");
    ui->status_box->setStyleSheet("QGroupBox { border:1px solid grey} ");
    last_requested_port = current_port;
  }
}

void WrapSensor::handleTimer() {
  if (m_time_last_received.isNull()) {
    return;
  }
  uint time_diff =
      QDateTime::currentDateTime().toTime_t() - m_time_last_received.toTime_t();
  if (time_diff <= 0) {
    return;
  }

  if (time_diff >= STALE_AGE) {

    ui->status_box->setStyleSheet("QGroupBox { border:1px solid grey} ");
    ui->wrapcount_label->setText("Unknown");
    ui->wrapheading_label->setText("Unknown");
  }

  else if (time_diff >= OLD_AGE) {
    ui->status_box->setStyleSheet("QGroupBox { border:1px solid yellow} ");
  } else {
    ui->status_box->setStyleSheet("QGroupBox { border:1px solid green} ");
  }

  ui->ss_update_label->setText(format_age(time_diff));
}

const QString WrapSensor::format_age(double seconds, double max) {

  if (seconds > max) {
    return QString("---");
  } else if (seconds > 3600) {
    const int hours = seconds / 3600;
    const int minutes = (seconds - (hours * 3600)) / 60;
    const int sec = (seconds - (hours * 3600 + minutes * 60));
    return QString("%1h %2m %3s").arg(hours).arg(minutes).arg(sec);
  } else if (seconds > 120) {
    const int minutes = seconds / 60;
    const int sec = (seconds - minutes * 60);
    return QString("%1m %2s").arg(minutes).arg(sec);
  } else {
    return QString("%1s").arg(seconds, 0, 'd', 0);
  }
}

} // namespace ros_wrap
} // namespace plugins
} // namespace navg

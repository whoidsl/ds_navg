/**
 * Copyright 2022 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 09/07/22
//

#ifndef DS_NAVG_ROS_WRAPSENSOR_H
#define DS_NAVG_ROS_WRAPSENSOR_H

#include <QLoggingCategory>
#include <QObject>
#include <QPointer>
#include <QSettings>
#include <QTime>
#include <QTimer>
#include <QWidget>
#include <QAction>
#include <dslmap/MapLayer.h>
#include <memory>
#include <navg/CorePluginInterface.h>
#include <ros/ros.h>
#include <ros/service_client.h>
#include <ros/console.h>

#include <ds_rqt_widgets/QtRosSubscription.h>
#include <ds_rqt_widgets/QtRosSubscriptionManager.h>

#include <ds_core_msgs/StringCmd.h>
#include <ds_sensor_msgs/WrapSensor.h>

#include "std_msgs/String.h"

namespace navg {
namespace plugins {
namespace ros_wrap {

namespace Ui {
class WrapSensor;
}

using LayerHash = QHash<QUuid, dslmap::MapLayer *>;

class WrapSensor : public QWidget, public navg::CorePluginInterface {
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "WrapSensor.json")

public:
  explicit WrapSensor(QWidget *parent = nullptr);
  ~WrapSensor() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings *settings) override;
  void saveSettings(QSettings &settings) override;

  // This plugin connects to DS_ROS_BACKEND
  void connectPlugin(const QObject *plugin,
                     const QJsonObject &metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction *> pluginMenuActions() override;
  QList<QAction *> pluginToolbarActions() override { return {}; }

signals:

  void settingsUpdated();

public slots:

  void showSettingsDialog();

protected:
  void setupPlugin();
  void setupConnections();

protected slots:
  void populateSmsPortDropdown();
  void updateWrapCount(AnyMessage);
  void updateFakeCount();
  void requestWraps();

private slots:
  void handleTimer();

private:
  const static QString format_age(double seconds, double max = 60*60*24*7);
  const uint OLD_AGE = 60;
  const uint STALE_AGE = 400;

  std::unique_ptr<Ui::WrapSensor> ui;
  QAction* m_action_show_settings = nullptr;
  QPointer<dslmap::MapView> m_view;

  ros::NodeHandlePtr node_handle;
  ros::NodeHandle nh;

  ros::ServiceClient request_wraps_srv;

  ds_rqt_widgets::QtRosSubscriptionManager *sub_manager;

  ds_rqt_widgets::QtRosSubscription *sub_wraps;

  //ros::Publisher chatter_pub;

  QString last_requested_port;
  
  int port1 = 2803; //10 meter
  int port2 = 2105; //11 meter
  int port3 = 2401; //100 meter

  QString wrap_sensor_node_name = "/jason/sensors/wrap_sensor";
  QScopedPointer<QTimer> age_timer;

  QDateTime m_time_last_received;
  
};

Q_DECLARE_LOGGING_CATEGORY(plugin_ros_wrap)
} // namespace ros_wrap
} // namespace plugins
} // namespace navg

#endif // DS_NAVG_ROS_WRAPSENSOR_H

/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/16/19.
//


#include "ros_backend.h"

#include <QCoreApplication>
#include <QSettings>

namespace navg {
namespace plugins {
namespace ros_backend {


Q_LOGGING_CATEGORY(plugin_ros_backend, "navg.plugins.ros.backend")

RosBackendPlugin::RosBackendPlugin(QWidget* parent) : QObject(parent)
{
  qDebug(plugin_ros_backend) <<"Starting ROS backend...";

  // we'll need int argc and char** argv; we'll keep arrays on the stack,
  // but its definitely annoying
  QStringList arguments = QCoreApplication::arguments();
  int argc = arguments.size();
  std::string arg_strs[argc]; // needed to make storage persistent
  char* argv[argc];
  // NOTE: This does NOT strip out any QT arguments, it passes everything to ROS.
  // This is definitely not ideal.

  qDebug(plugin_ros_backend) <<"ARGC: " <<argc;
  for (size_t i=0; i<argc; i++) {
    arg_strs[i] = arguments[i].toStdString();
    // in generaly, this isn't really OK.  But ROS is super-slopy about
    // constness, so we use this const_cast to fix it
    argv[i] = const_cast<char*>(arg_strs[i].c_str());

    qDebug(plugin_ros_backend) <<"ARGV[" <<i<<"]: " <<argv[i];
  }

  // Ok, we now have all our command-line arguments.  Let's actually start ROS
  qDebug(plugin_ros_backend) <<"Starting ros::init...";
  ros::init(argc, argv, "navg", ros::init_options::AnonymousName);

  // grab a node_handle that persists as long as this node is running
  node_handle.reset(new ros::NodeHandle());

  // start our eventloop
  qDebug(plugin_ros_backend) <<"Starting ROS eventloop...";
  ros_spinner = std::make_shared<ros::AsyncSpinner>(2);
  ros_spinner->start();
  qDebug(plugin_ros_backend) <<"\nROS Eventloop Running!!\n\n";
}

RosBackendPlugin::~RosBackendPlugin() {
  ros_spinner->stop();
}

void
RosBackendPlugin::setMapView(QPointer<dslmap::MapView> view)
{
  // don't do anythign
}

void
RosBackendPlugin::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  // TODO: Connect to another plugin
}

void
RosBackendPlugin::loadSettings(QSettings*)
{
}

void
RosBackendPlugin::saveSettings(QSettings& settings)
{
  // we have to create an entry in the ini file so that
  // this gets loaded at all.  Dummy is fine.
  settings.setValue("loadme", "any_value_at_all");
}

QList<QAction*>
RosBackendPlugin::pluginMenuActions()
{
  return {};
}

} // hdg_depth_alt
} // plugins
} // navg
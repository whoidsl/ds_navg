/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ivaughn on 5/16/19.
//

#ifndef DS_NAVG_ROS_LATLON_H
#define DS_NAVG_ROS_LATLON_H

#include <dslmap/BeaconLayer.h>
#include <dslmap/MapItem.h>
#include <dslmap/Proj.h>
#include <navg/CorePluginInterface.h>
#include <dslmap/MapLayer.h>

#include <QAction>
#include <QHash>
#include <QJsonObject>
#include <QLabel>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QUuid>
#include <QWidget>
#include <memory>

#include "ros_beacon_layer.h"
#include "ros_latlon_listener.h"

namespace navg {
namespace plugins {
namespace ros_latlon {

namespace Ui {
class LatLon;
}
using LayerHash = QHash<QUuid, dslmap::MapLayer *>;

class LatLon : public QWidget, public navg::CorePluginInterface {
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "LatLon.json")

signals:

public:
  explicit LatLon(QWidget *parent = nullptr);
  ~LatLon() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings *settings) override;
  void saveSettings(QSettings &settings) override;

  // This plugin connects to DS_ROS_BACKEND
  void connectPlugin(const QObject *plugin,
                     const QJsonObject &metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction *> pluginMenuActions() override;
  QList<QAction *> pluginToolbarActions() override { return {}; }

protected slots:

  void selectTopic(QString topic);
  void setupTopicBox();
  /// \brief Set the current position based on the latest fix
  void handleFixMessage(DsNavgLatLonFix fix, QUuid layer_id);
  /// \brief Create beacon layer with default projection
  RosBeaconLayer *createRosBeaconLayer();

  void populateLayerList();
  /// \brief Will assign current selected topic to layer and set projection
  void assignTopicToCurrentLayer();
  void clearTrail();
  void populateCurrentTopicText(QString name);
  QString getCurrentTopicQString();
  RosBeaconLayer *getCurrentRosMapLayer();
  RosBeaconLayer *getLayerById(QUuid id);
  LayerHash getAllRosMapLayers();
  void sceneLayerRemoved(dslmap::MapLayer *layer);

private:
  std::unique_ptr<Ui::LatLon> ui;
  QPointer<dslmap::MapView> m_view;
  QString current_selected_topic_qstring;
  QString initial_topic;
  ros::NodeHandlePtr node_handle;
  QMap<QUuid, QPointer<LatlonListener>> m_layer_listener_map;
  QPointer<LatlonListener> listener;
  int m_seconds_since;
  // if you forgot to name your beacon layer...this will help distinguish
  static int default_beacon_layer_count;
  bool isCurrentLayerValid();

  ///This exists to confirm if when a Beacon Layer is created, no projection 
  //was set in scene beforehand. 
  bool wasNoSceneProjection=false;

  void removeDuplicateTopics(std::vector<std::string> &topics);
};

Q_DECLARE_LOGGING_CATEGORY(plugin_ros_latlon)
} // namespace ros_latlon
} // namespace plugins
} // namespace navg

#endif // DS_NAVG_ROS_LATLON_H

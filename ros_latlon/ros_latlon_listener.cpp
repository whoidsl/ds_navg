/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/16/19.
//

#include "ros_latlon_listener.h"
#include "ros_latlon_types.hpp"

#include <ros/ros.h>
#include <sensor_msgs/NavSatFix.h>

namespace navg {
namespace plugins {
namespace ros_latlon {


// ///////////////////////////////////////////////////////////////////////// //
// Actual LatlonListener implementation
LatlonListener::LatlonListener(QObject* parent) : QObject(parent) {}

// this needs to be declared separately.  Thanks, C++.
std::vector<std::shared_ptr<SubscriberType>> LatlonListener::knownTypes;

bool LatlonListener::topicListenable(const ros::master::TopicInfo &topic) const {
  for (const auto& t : knownTypes) {
    if (t->typeString() == topic.datatype) {
      return true;
    }
  }
}

std::vector<std::string> LatlonListener::getAvailableTopics() const {
  ros::master::V_TopicInfo topics;
  ros::master::getTopics(topics);
  std::vector<std::string> ret;

  for (auto t : topics) {
    if (topicListenable(t)) {
      ret.push_back(t.name);
    }
  }

  return ret;
}

void LatlonListener::setSubscription(ros::NodeHandle& nh, const std::string &topicname) {
  ros::master::V_TopicInfo topics;
  ros::master::getTopics(topics);

  // First, get the topicinfo that corresponds to this topic
  ros::master::TopicInfo topicToSubscribe;
  bool found = false;
  for (auto t : topics) {
    if (t.name == topicname) {
      topicToSubscribe = t;
      found = true;
      break;
    }
  }
  if (!found) {
    ROS_ERROR_STREAM("Unable to find topic " <<topicname);
    return;
  }

  // Ok, we now have complete topic info.  Let's subscribe
  for (const std::shared_ptr<SubscriberType>& t : knownTypes) {
    if (t->typeString() == topicToSubscribe.datatype) {
      sub = t->subscribe(nh, topicname, this);
      m_topicname = topicname;
    }
  }
}

void LatlonListener::registerType(std::shared_ptr<SubscriberType> typeptr) {

  // first, avoid adding duplicates
  for (const auto& t : knownTypes) {
    if (t->typeString() == typeptr->typeString()) {
      ROS_WARN_STREAM("Attempting to add duplicate type " <<t->typeString());
      break;
    }
  }

  knownTypes.push_back(typeptr);
}

void LatlonListener::emitFix(DsNavgLatLonFix fix) {
  fix.topicname = m_topicname;
  emit fixReceived(fix);
  emit fixIdReceived(fix, this->id);
}


std::string LatlonListener::getTopicName(){
  return m_topicname;
}

void LatlonListener::setId(QUuid id){
  this->id = id; 
}

} // ros_latlon
} // plugins
} // navg
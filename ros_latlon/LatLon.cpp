/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ivaughn on 5/16/19.
//

#include "LatLon.h"
#include "ui_LatLon.h"
#include <QPushButton>
#include <QSettings>
#include <dslmap/BeaconLayer.h>
#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>
#include <dslmap/MarkerSymbol.h>
#include <dslmap/Proj.h>
#include <ros/ros.h>

namespace navg {
namespace plugins {
namespace ros_latlon {

Q_LOGGING_CATEGORY(plugin_ros_latlon, "navg.plugins.ros.latlon")

LatLon::LatLon(QWidget *parent)
    : QWidget(parent), ui(std::make_unique<Ui::LatLon>()), m_seconds_since(-1) {
  // make signals & slots actually work
  qRegisterMetaType<DsNavgLatLonFix>();

  ui->setupUi(this);
  ui->current_layer_topic_text->setReadOnly(true);
  ui->beacon_name->setPlaceholderText("Enter Layer Name");

  connect(ui->create_beacon_button, &QPushButton::clicked, this,
          &LatLon::createRosBeaconLayer);

  connect(ui->test_ros_button, &QPushButton::clicked, this,
          &LatLon::assignTopicToCurrentLayer);

  connect(ui->clear_trail_button, &QPushButton::clicked, this,
          &LatLon::clearTrail);

  connect(ui->beacon_combo,
          static_cast<void (QComboBox::*)(const QString &)>(
              &QComboBox::currentIndexChanged),
          this, &LatLon::populateCurrentTopicText);
}

LatLon::~LatLon() = default;

int LatLon::default_beacon_layer_count = 0;

void LatLon::setMapView(QPointer<dslmap::MapView> view) {
  m_view = view;
  auto m_scene = view->mapScene();
  if (m_scene) {
    connect(m_scene,
            static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer *)>(
                &dslmap::MapScene::layerRemoved),
            this, &LatLon::sceneLayerRemoved);
  }
}

void LatLon::connectPlugin(const QObject *plugin, const QJsonObject &metadata) {
  if (!plugin) {
    return;
  }

  const auto name =
      metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  // connect to ROS backend plugin
  if (name == "dsros_Backend") {
    qDebug(plugin_ros_latlon) << "Connecting lat/lon plugin to ROS backend";
    node_handle.reset(new ros::NodeHandle());
    listener = new LatlonListener();
    setupTopicBox();

    connect(ui->topicBox,
            static_cast<void (QComboBox::*)(const QString &)>(
                &QComboBox::currentIndexChanged),
            this, &LatLon::selectTopic);

    connect(ui->refreshTopics, &QPushButton::released, this,
            &LatLon::setupTopicBox);
  }
}

void LatLon::loadSettings(QSettings *settings) {}

void LatLon::saveSettings(QSettings &settings) {}

QList<QAction *> LatLon::pluginMenuActions() { return {}; }

void LatLon::selectTopic(QString topic) {

  current_selected_topic_qstring = topic;
}

QString LatLon::getCurrentTopicQString() { return ui->topicBox->currentText(); }
RosBeaconLayer *LatLon::getCurrentRosMapLayer() {

  if (ui->beacon_combo->count() <= 0) {
    return nullptr;
  }
  auto curr_index = ui->beacon_combo->currentIndex();

  QVariant variant = ui->beacon_combo->itemData(curr_index);
  RosBeaconLayer *beacon_layer = nullptr;
  if (variant.canConvert<RosBeaconLayer *>()) {

    beacon_layer = variant.value<RosBeaconLayer *>();

    return beacon_layer;
  } else {
    qCDebug(plugin_ros_latlon) << "Conversion to RosBeaconLayer failed";
    return nullptr;
  }
}

void LatLon::assignTopicToCurrentLayer() {

  if (!isCurrentLayerValid()) {
    return;
  }
  auto topic = getCurrentTopicQString();
  auto topic_str = topic.toStdString();
  RosBeaconLayer *layer = getCurrentRosMapLayer();
  qWarning(plugin_ros_latlon) << "Current selected topic is " << topic;

  if (!layer) {
    qWarning(plugin_ros_latlon) << "Layer not found";
    return;
  }

  if (listener) {
    qWarning(plugin_ros_latlon) << "Listener valid";
    QUuid id = layer->id();
    if (m_layer_listener_map[id]) {
      qWarning(plugin_ros_latlon) << "Listener already set for this layer";

      if (layer->getRosTopic() == topic) {
        qWarning(plugin_ros_latlon) << "Layer is already listening to " << topic
                                    << " .No need to set new listener";
        return;

      } else {
        // delete the current listener stored in map. Can keep key since we are
        // reassigning
        LatlonListener *curr_listener = m_layer_listener_map.value(id);
        qWarning(plugin_ros_latlon) << "Deleting current listener from map";
        delete curr_listener;
      }
    }

    layer->setRosTopic(topic); // only set the layer topic after checking the
                               // previous layer values

    // should reassign it here if already exists listener. can still reset to
    // new listener for diff topic

    qDebug(plugin_ros_latlon) << "Connecting listener to topic " << topic;
    m_layer_listener_map[id] = new LatlonListener();
    m_layer_listener_map[id]->setId(id);
    m_layer_listener_map[id]->setSubscription(*node_handle, topic_str);

    populateCurrentTopicText(
        layer->name()); // could likely also emit topic changed instead

    qDebug(plugin_ros_latlon)
        << "Listener is currently subscribed to  "
        << QString::fromStdString(m_layer_listener_map[id]->getTopicName());

    connect(m_layer_listener_map[id], &LatlonListener::fixIdReceived, this,
            &LatLon::handleFixMessage, Qt::QueuedConnection);

  }

  else {
    qWarning(plugin_ros_latlon)
        << "Plugin not connected to ROS! Not listening to topic.";
  }
}

LayerHash LatLon::getAllRosMapLayers() {
  auto scene = m_view->mapScene();

  if (!scene) {
    qCDebug(plugin_ros_latlon) << "No map scene associated with view.";
    return LayerHash();
  }
  return scene->layers(dslmap::MapItemType::RosBeaconLayerType);
}

void LatLon::removeDuplicateTopics(std::vector<std::string> &topics) {
  sort(topics.begin(), topics.end());
  std::vector<std::string> sorted;
  // need test for if vector too small or for last value
  for (int i = 1; i < topics.size(); i++)
    if (topics[i - 1] != topics[i])

      sorted.push_back(topics[i - 1]);
}
void LatLon::setupTopicBox() {
  if (!listener) {
    return;
  }

  // get the list of available topics
  std::vector<std::string> topics = listener->getAvailableTopics();

  if (topics.size() == 0) {
    qDebug(plugin_ros_latlon) << "No valid topics detected!";
    return;
  }

  // get the current topic
  QString currentTopic = ui->topicBox->currentText();
  qDebug(plugin_ros_latlon) << "Current topic: " << currentTopic;

  bool current_found = false;
  bool initial_found = false;

  // setup our topics
  bool signalsBlocked = ui->topicBox->blockSignals(true);
  ui->topicBox->clear();
  for (auto topic : topics) {
    QString qtopic = QString::fromStdString(topic);
    qDebug(plugin_ros_latlon) << "Adding topic: " << qtopic;
    ui->topicBox->addItem(qtopic);
    if (!currentTopic.isEmpty() && qtopic == currentTopic) {
      current_found = true;
    }
    if (!initial_topic.isEmpty() && qtopic == initial_topic) {
      initial_found = true;
    }
  }

  // set the current topic, preserving state if possible
  if (!currentTopic.isEmpty() && current_found) {
    ui->topicBox->setCurrentText(currentTopic);
  } else if (!initial_topic.isEmpty() && initial_found) {
    ui->topicBox->setCurrentText(initial_topic);
    selectTopic(initial_topic);
  }

  // restore state
  ui->topicBox->blockSignals(signalsBlocked);
}
RosBeaconLayer *LatLon::createRosBeaconLayer() {
  auto scene = m_view->mapScene();

  if (!scene) {
    qCDebug(plugin_ros_latlon) << "No map scene associated with view.";
    return nullptr;
  }

  auto proj = scene->projection();
  if (!proj) {
    wasNoSceneProjection = true;
    // we need a default dummy projection for the mapscene to add new layers
    proj = dslmap::Proj::fromDefinition(
        "+proj=tmerc +datum=WGS84 +units=m +ellps=WGS84 +towgs84=0,0,0");
    qCInfo(plugin_ros_latlon)
        << "Map scene has no projection yet; setting to default";
    scene->setProjection(proj);
  }

  // create layer
  qCInfo(plugin_ros_latlon) << "Creating new layer";
  auto layer = new RosBeaconLayer();
  layer->setProjection(proj);

  auto beaconSymbol = std::make_shared<dslmap::ShapeMarkerSymbol>();
  beaconSymbol->setFillColor(Qt::blue);
  beaconSymbol->setOutlineColor(Qt::red);
  beaconSymbol->setShape(dslmap::ShapeMarkerSymbol::Shape::Arrow);
  layer->setBeaconSymbol(beaconSymbol);
  layer->setBeaconVisible(true);

  auto trailSymbol = std::make_shared<dslmap::ShapeMarkerSymbol>();
  trailSymbol->setFillColor(Qt::red);
  trailSymbol->setFillStyle(Qt::SolidPattern);
  trailSymbol->setOutlineColor(Qt::red);
  trailSymbol->setSize(0.5);
  trailSymbol->setMinimumSize(0.1);
  trailSymbol->setScaleInvariant(true);
  trailSymbol->setShape(dslmap::ShapeMarkerSymbol::Shape::Circle);
  layer->setTrailSymbol(trailSymbol);
  layer->setTrailVisible(true);
  layer->setCapacity(100 * 3600);
  layer->setVisible(true);
  // END ROS BEACON GUI SETTINGS

  if (ui->beacon_name->text() == "") {
    QString name = "Ros Beacon Layer";
    name.append(QString::number(default_beacon_layer_count));
    default_beacon_layer_count++;
    layer->setName(name);
  } else {
    layer->setName(ui->beacon_name->text());
  }
  scene->addLayer(layer);
  // helper function to update layer list. currently implemented as qcombo
  populateLayerList();
  return layer;
}

void LatLon::populateLayerList() {
  ui->beacon_combo->clear();
  auto layers = getAllRosMapLayers();

  for (auto layer : layers) {
    QString name = layer->name();
    ui->beacon_combo->addItem(name, QVariant::fromValue(layer));
  }
}

void LatLon::clearTrail() {
  if (!isCurrentLayerValid()) {
    return;
  }
  auto layer = getCurrentRosMapLayer();
  ;
  qCDebug(plugin_ros_latlon)
      << "current layer  " << layer->name() << " clearing trail";
  layer->clearTrail();
}

void LatLon::populateCurrentTopicText(QString name) {
  ui->current_layer_topic_text->clear();

  auto current_layer = getCurrentRosMapLayer();
  if (!current_layer)
    return;

  ui->current_layer_topic_text->setText(current_layer->getRosTopic());
}

bool LatLon::isCurrentLayerValid() {
  if (ui->beacon_combo->count() <= 0) {
    return false;
  }
  if (getCurrentRosMapLayer() == nullptr) {
    false;
  }
  return true;
}

void LatLon::handleFixMessage(DsNavgLatLonFix fix, QUuid fix_id) {

  auto layer = getLayerById(fix_id);
  if (!layer) {
    qCInfo(plugin_ros_latlon) << "Layer not valid";
    return;
  }

  // use first valid incoming fix to reset layer projection
  auto scene = m_view->mapScene();

  if (!scene) {
    qCDebug(plugin_ros_latlon) << "No map scene associated with view.";
    return;
  }
  // we know a projection must exist, since we defaulted when creating layer
  // now we update the projection to use real coordinates and align correctly
  // once
  if (wasNoSceneProjection) {

    qCDebug(plugin_ros_latlon) << "Setting the proj once.";
    std::shared_ptr<const dslmap::Proj> proj;

    QString projString = "+proj=tmerc +datum=WGS84 +units=m +lon_0=" +
                         QString::number(fix.lon, 'f', 7) +
                         " +lat_0=" + QString::number(fix.lat, 'f', 7) +
                         " +ellps=WGS84 +towgs84=0,0,0";
    proj = dslmap::Proj::fromDefinition(projString);

    scene->setProjection(proj);
    layer->setProjection(proj);

    wasNoSceneProjection = false;
  }

  qDebug(plugin_ros_latlon)
      << "Got Fix: " << QString::number(fix.lat, 'f', 7) << ", "
      << QString::number(fix.lon, 'f', 7) << " from " << layer->getRosTopic()
      << " on id " << fix_id << QString::fromStdString(fix.topicname)
      << " and heading is " << -fix.heading;

  auto tstamp = QDateTime::fromMSecsSinceEpoch(
      static_cast<qint64>(fix.time.toNSec() / 1000000));
  if (std::isfinite(fix.heading)) {
    layer->updatePositionLonLat(fix.lon, fix.lat, tstamp, &fix.heading);
  } else {
    layer->updatePositionLonLat(fix.lon, fix.lat, tstamp);
  }
}

RosBeaconLayer *LatLon::getLayerById(QUuid id) {
  RosBeaconLayer *matched_layer;

  auto layers = getAllRosMapLayers();
  for (auto layer : layers) {
    if (layer->id() == id) {
      matched_layer = qobject_cast<RosBeaconLayer *>(layer);
      break;
    }
  }
  return matched_layer;
}

void LatLon::sceneLayerRemoved(dslmap::MapLayer *layer) {
  if (layer->type() != dslmap::MapItemType::RosBeaconLayerType) {
    return;
  }
  auto curr_nav_layer = dynamic_cast<RosBeaconLayer *>(layer);
  if (!curr_nav_layer) {
    return;
  }
  QUuid layer_id = layer->id();
  int combo_size = ui->beacon_combo->count();

  // deleting layer from combo box
  for (int i = 0; i < combo_size; i++) {
    QVariant variant = ui->beacon_combo->itemData(i);
    RosBeaconLayer *combo_layer = nullptr;
    if (variant.canConvert<RosBeaconLayer *>()) {
      combo_layer = variant.value<RosBeaconLayer *>();
      qDebug() << "Deleting beacon combo data ";
      ui->beacon_combo->removeItem(i);
      break;

    } else {
      continue;
    }
  }

  // delete layer key and value from map
  for (const auto t : m_layer_listener_map.keys()) {
    if (t == layer->id()) {
      qDebug() << "Delete listener and layer ";

      delete m_layer_listener_map[t];
      m_layer_listener_map.remove(t);
      break;
    }
  }
}

} // namespace ros_latlon
} // namespace plugins
} // namespace navg

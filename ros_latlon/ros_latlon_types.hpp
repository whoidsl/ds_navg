/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/17/19.
//

#ifndef DS_NAVG_ROS_LATLON_TYPES_HPP
#define DS_NAVG_ROS_LATLON_TYPES_HPP


#include <ros/ros.h>

#include "ros_latlon_listener.h"

namespace navg {
namespace plugins {
namespace ros_latlon {

// ///////////////////////////////////////////////////////////////////////// //
// SubscriberType overhead
class SubscriberType {

 public:
  virtual ~SubscriberType() = default;

  virtual std::string typeString() const = 0;
  virtual ros::Subscriber subscribe(ros::NodeHandle &nh, const std::string &topicname, LatlonListener* obj) = 0;
};

template<typename T>
class SubscriberTypeT : public SubscriberType {
 public:

  virtual ~SubscriberTypeT() = default;

  virtual std::string typeString() const {
    return ros::message_traits::DataType<T>::value();
  }

  ros::Subscriber subscribe(ros::NodeHandle &nh, const std::string& topicname, LatlonListener* cb_obj) override {
    return nh.subscribe<T>(topicname, 10, boost::bind(&SubscriberTypeT<T>::message_callback, cb_obj, _1));
  }

  /// This is the message callback; it needs to get specialized for every type
  static DsNavgLatLonFix convert_message(const T& msg);

  /// Callback that fires when a new message comes in.  Just convert to
  /// a LatLonFix and fire off.
  static void message_callback(LatlonListener* obj, const boost::shared_ptr<const T>& msg) {
    DsNavgLatLonFix fix = convert_message(*msg);

    if (obj) {
      obj->emitFix(fix);
    } else {
      ROS_ERROR("LatLonSubscriber got a message but has no Listener set!");
    }
  }
};

// this macro SHAMELESSLY steals from ROS's ClassLoader
#define INNER_LATLON_REGISTER_MSG_TYPE(MessageType, UniqueID) \
namespace\
{ \
struct LatlonProxyExec ## UniqueID \
{ \
  LatlonProxyExec ## UniqueID() \
  { \
    LatlonListener::registerType(std::shared_ptr<SubscriberType>(new SubscriberTypeT<MessageType>())); \
  } \
}; \
static LatlonProxyExec ## UniqueID g_register_type_ ## UniqueID; \
}

// We need to wrap the INNER_LATLON_REGISTER_MSG_TYPE macro to ensure that
// __COUNTER__ gets expanded correctly.  Great moments in C++
// pre-processing.
#define INNER_LATLON_EXPAND_COUNTER(MessageType, UniqueID) \
  INNER_LATLON_REGISTER_MSG_TYPE(MessageType, UniqueID)


#define LATLON_REGISTER_MSG_TYPE(MessageType) \
  INNER_LATLON_EXPAND_COUNTER(MessageType, __COUNTER__)

} // ros_latlon
} // plugins
} // navg

#endif //DS_NAVG_ROS_LATLON_TYPES_HPP

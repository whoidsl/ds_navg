# How to add new types

New ROS types must provide a latitude, longitude, and ideally time.  Here's to add them
to the list of types available in the latlon plugin.  Let's say we're adding the following new ROS type `my_sensors::MyLatLon`:

```json
time fixTime
float64 lat
float64 lon
float64 dep
```

Of course, for this you really SHOULD be using a standard message like `sensor_msgs::NavSatFix`, but this is supposed to
be a minimal example.  Here's how to add it:

1. Open `ros_latlon_conversions.hpp`
1. Add your `#include` near the top of the file.  In this case `#include <my_sensors/MyLatLon.h>`
1. Further down, add a `LATLON_REGISTER` to add your type to the list of known types with 
`LATLON_REGISTER_MSG_TYPE(my_sensors::MyLatLon)`
1. Add a template specialization of the `convert_message` function that's used to convert to the QT-ready type.  It 
will work fine, just be sure to copy the function signature _exactly_ and substitute your types.

```
// declare a template specializtion that makes this actually work
template<>
DsNavgLatLonFix SubscriberTypeT<my_sensors::MyLatLon>::convert_message(const my_sensors::MyLatLon& msg) {
  DsNavgLatLonFix ret;

  ret.time = msg.time
  ret.lat = msg.lat
  ret.lon = msg.lon
  ret.altitude = -msg.dep

  return ret;
}
```

This function is supposed to be trivial; that's the whole idea!

## How do I actually use this type though?

That's it; recompile and you're done!  Topics with the new type will automatically appear in the dropdown menu.
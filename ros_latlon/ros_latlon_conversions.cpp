/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/17/19.
//

#ifndef DS_NAVG_ROS_LATLON_CONVERSIONS_HPP
#define DS_NAVG_ROS_LATLON_CONVERSIONS_HPP

#include <limits>
#include <cmath>

#include "ros_latlon_types.hpp"
// This file contains converters to translate various ROS message types to
// QT-ready fix messages

// This should happen outside the namespace block
#include <sensor_msgs/NavSatFix.h>
#include <ds_nav_msgs/NavState.h>
#include <QString>

namespace navg {
namespace plugins {
namespace ros_latlon {

// ///////////////////////////////////////////////////////////////////////// //
// sensor_msgs::NavSatFix

// register the type
LATLON_REGISTER_MSG_TYPE(sensor_msgs::NavSatFix);

// declare a template specializtion that makes this actually work
template<>
DsNavgLatLonFix SubscriberTypeT<sensor_msgs::NavSatFix>::convert_message(const sensor_msgs::NavSatFix& msg) {
  DsNavgLatLonFix ret;

  ret.time = msg.header.stamp;
  ret.lat = msg.latitude;
  ret.lon = msg.longitude;
  ret.altitude = msg.altitude;
  ret.heading = std::numeric_limits<double>::quiet_NaN();

  return ret;
}

// ///////////////////////////////////////////////////////////////////////// //
// ds_nav_msgs::NavState
LATLON_REGISTER_MSG_TYPE(ds_nav_msgs::NavState);

template<>
DsNavgLatLonFix SubscriberTypeT<ds_nav_msgs::NavState>::convert_message(const ds_nav_msgs::NavState& msg) {
  DsNavgLatLonFix ret;

  ret.time = msg.header.stamp;
  ret.lat = msg.lat;
  ret.lon = msg.lon;
  ret.altitude = -msg.down;
  //ret.heading = msg.heading*180.0/M_PI;
  ret.heading = -msg.heading;
  

  return ret;
}

} // ros_latlon
} // plugins
} // navg

#endif //DS_NAVG_ROS_LATLON_CONVERSIONS_HPP

/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 5/16/19.
//

#ifndef DS_NAVG_ROS_LATLON_LISTENER_H
#define DS_NAVG_ROS_LATLON_LISTENER_H

#include <ros/ros.h>

#include <string>
#include <QObject>
#include <QUuid>

/// A simple lat/lon fix.  Define separate with a basic class
/// because passing generic messages through QT and templates don't
/// work.  This has to be done at global scope because QT, so we add
// some namespacey tags
struct DsNavgLatLonFix {
  ros::Time time;
  std::string topicname;
  double lat;
  double lon;
  double altitude;
  double heading;
};

Q_DECLARE_METATYPE(DsNavgLatLonFix);

namespace navg {
namespace plugins{
namespace ros_latlon {

// forward declaration
class SubscriberType;

/// Generic class to listen for lat/lon data and convert to a QT-ready type
class LatlonListener : public QObject {
  Q_OBJECT

 public:
  LatlonListener(QObject* parent=0);

  bool topicListenable(const ros::master::TopicInfo& topic) const;
  std::vector<std::string> getAvailableTopics() const;
  void setSubscription(ros::NodeHandle& nh, const std::string& topicname);

  void emitFix(DsNavgLatLonFix fix);

  static void registerType(std::shared_ptr<SubscriberType> typeptr);
  std::string getTopicName();
  void setId(QUuid id);

 signals:
  void fixReceived(DsNavgLatLonFix fix);
  void fixIdReceived(DsNavgLatLonFix fix, QUuid id);

 protected:
  ros::Subscriber sub;
  std::string m_topicname;
  static std::vector<std::shared_ptr<SubscriberType>> knownTypes;
  QUuid id;
};

} // namespace ros_latlon
} // namespace plugins
} // namespace navg

#endif //DS_NAVG_ROS_LATLON_LISTENER_H
